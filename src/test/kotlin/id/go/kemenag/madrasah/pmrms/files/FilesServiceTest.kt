package id.go.kemenag.madrasah.pmrms.files

import id.go.kemenag.madrasah.pmrms.files.constant.ATTACHMENT_TYPE_FOTO
import id.go.kemenag.madrasah.pmrms.files.helpers.buildMultipartFile
import id.go.kemenag.madrasah.pmrms.files.model.request.DeleteDataRequest
import id.go.kemenag.madrasah.pmrms.files.model.request.UploadRequest
import id.go.kemenag.madrasah.pmrms.files.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.files.pojo.Files
import id.go.kemenag.madrasah.pmrms.files.service.FilesService
import org.assertj.core.api.Assertions
import org.easymock.EasyMock
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.MethodOrderer
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestMethodOrder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import java.io.File
import javax.servlet.http.HttpServletResponse


@Suppress("UNCHECKED_CAST", "RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
@TestMethodOrder(MethodOrderer.MethodName::class)
@SpringBootTest
class FilesServiceTest {

    @Autowired
    private lateinit var service: FilesService

    companion object {
        var createdId = ""
    }

    @Test
    @DisplayName("Test Upload")
    fun test1() {
        println("Test Upload")
        val classLoader = this.javaClass.classLoader
        val file = File(classLoader.getResource("files/test/test.png").file)
        val request = UploadRequest()
        val multipart = buildMultipartFile(file, "image/png")
        multipart?.let {
            request.files = mutableListOf(multipart)
        }
        request.attachmentType = ATTACHMENT_TYPE_FOTO

        val test: ResponseEntity<ReturnData> = service.upload(request)
        val returnData: ReturnData? = test.body
        val uploaded: List<Files> = returnData?.data as List<Files>

        createdId = uploaded[0].id.toString()

        Assertions.assertThat(test.statusCode).isNotEqualTo(HttpStatus.INTERNAL_SERVER_ERROR)
    }

    @Test
    @DisplayName("Test Detail")
    fun test2() {
        println("Test Detail")
     /*   val test: ByteArray? = service.getDetail(createdId, httpServletResponse)
        Assertions.assertThat(test).isNotEqualTo(null)*/
    }

    @Test
    @DisplayName("Test Download")
    fun test3() {
        println("Test Download")
        val mockResponse: HttpServletResponse = EasyMock.createMock(HttpServletResponse::class.java)
        val test: ByteArray? = service.download(createdId, mockResponse)
        Assertions.assertThat(test).isNotEqualTo(null)
    }

    @Test
    @DisplayName("Test Delete")
    fun test4() {
        println("Test Delete")
        val test: ResponseEntity<ReturnData> = service.deleteData(DeleteDataRequest(createdId))
        Assertions.assertThat(test).isNotEqualTo(null)
    }
}
