package id.go.kemenag.madrasah.pmrms.files.repository

import id.go.kemenag.madrasah.pmrms.files.pojo.AttachmentType
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import java.util.*

interface AttachmentTypeRepository : JpaRepository<AttachmentType, String> {

    fun findByTypeAndActive(@Param("type") type: Int?, active: Boolean = true) : Optional<AttachmentType>
}
