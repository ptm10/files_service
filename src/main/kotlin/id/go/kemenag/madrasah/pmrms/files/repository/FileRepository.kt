package id.go.kemenag.madrasah.pmrms.files.repository

import id.go.kemenag.madrasah.pmrms.files.pojo.Files
import org.springframework.data.jpa.repository.JpaRepository

interface FileRepository : JpaRepository<Files, String> {
}
