package id.go.kemenag.madrasah.pmrms.files.helpers

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import id.go.kemenag.madrasah.pmrms.files.constant.UPLOAD_DIR
import id.go.kemenag.madrasah.pmrms.files.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.files.model.users.Users
import id.go.kemenag.madrasah.pmrms.files.pojo.AttachmentType
import id.go.kemenag.madrasah.pmrms.files.pojo.Files
import org.apache.commons.io.FileUtils
import org.apache.commons.io.FilenameUtils
import org.apache.commons.io.IOUtils
import org.springframework.core.env.Environment
import org.springframework.mock.web.MockMultipartFile
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.context.support.WebApplicationContextUtils
import org.springframework.web.multipart.MultipartFile
import reactor.util.annotation.NonNull
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.net.MalformedURLException
import java.net.URL
import java.time.LocalDate
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import java.nio.file.Files as NioFIles


fun getUserLogin(): Users? {
    return try {
        val principal = SecurityContextHolder.getContext().authentication.principal as Any
        val objectMapper = ObjectMapper()
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        objectMapper.readValue(principal.toString(), Users::class.java)
    } catch (e: Exception) {
        null
    }
}

@Throws(MalformedURLException::class)
fun getBaseUrl(request: HttpServletRequest): String {
    try {
        val requestURL = URL(request.requestURL.toString())
        val port = if (requestURL.port == -1) "" else ":" + requestURL.port
        return requestURL.protocol + "://" + requestURL.host + port + request.contextPath + "/"
    } catch (e: Exception) {
        throw e
    }
}

fun getFullUrl(request: HttpServletRequest): String {
    val requestURL = StringBuilder(request.requestURL.toString())
    val queryString = request.queryString
    return if (queryString == null) {
        requestURL.toString()
    } else {
        requestURL.append('?').append(queryString).toString()
    }
}

fun uploadFile(
    multipart: MultipartFile,
    path: String,
    attachmentType: AttachmentType,
): Files {
    var fos: FileOutputStream? = null
    try {
        val fileName =
            System.currentTimeMillis().toString() + "-" + multipart.originalFilename?.trim()?.replace(" ", "-")

        val currentDate = LocalDate.now()
        val pathFile = path + File.separator + currentDate.year + File.separator + currentDate.month

        val dir = File(UPLOAD_DIR + pathFile)
        if (!dir.exists()) {
            dir.mkdirs()
        }

        val convFile = File(dir.absolutePath + File.separator + fileName)
        fos = FileOutputStream(convFile)
        fos.write(multipart.bytes)
        fos.close()

        return Files(
            filepath = pathFile,
            filename = fileName,
            fileExt = FilenameUtils.getExtension(fileName),
            attachmentTypeId = attachmentType.id,
            attachmentType = attachmentType,
            mimeType = multipart.contentType
        )
    } catch (e: Exception) {
        throw RuntimeException("Gagal melakukan proses upload ${multipart.originalFilename}. Error: " + e.message)
    } finally {
        fos?.close()
    }
}

fun deleteFile(fileDir: String) {
    try {
        val delete = File(fileDir)
        if (delete.exists()) {
            FileUtils.forceDelete(delete)
        }
    } catch (e: Exception) {
        throw e
    }
}

@Throws(IOException::class)
fun buildMultipartFile(
    @NonNull file: File,
    @NonNull multipartFileParameterName: String?
): MultipartFile? {
    var multipartFile: MultipartFile?
    FileInputStream(file).use { input ->
        multipartFile = MockMultipartFile(
            multipartFileParameterName ?: "",
            file.name,
            NioFIles.probeContentType(file.toPath()),
            IOUtils.toByteArray(input)
        )
    }
    return multipartFile
}

fun getEnvFromHttpServletRequest(httpServletRequest: HttpServletRequest, key: String): String {
    var rData = ""
    try {
        val servletContext = httpServletRequest.session.servletContext
        val webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext)
        val env: Environment = webApplicationContext!!.getBean(Environment::class.java)
        rData = env.getProperty(key, "")
    } catch (_: Exception) {

    }
    return rData
}

fun responseNotFound(response: HttpServletResponse, message: String = "not found") {
    val json = ObjectMapper().writeValueAsString(
        ReturnData(
            message = message
        )
    )

    response.contentType = "application/json"
    response.status = HttpServletResponse.SC_NOT_FOUND
    response.writer.write(json)
    response.writer.flush()
    response.writer.close()
}
