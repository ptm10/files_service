package id.go.kemenag.madrasah.pmrms.files.controller

import id.go.kemenag.madrasah.pmrms.files.model.request.DeleteDataRequest
import id.go.kemenag.madrasah.pmrms.files.model.request.UploadRequest
import id.go.kemenag.madrasah.pmrms.files.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.files.service.FilesService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletResponse
import javax.validation.Valid


@Api(tags = ["Files"], description = "Files API")
@RestController
@RequestMapping(path = ["files"])
class FilesController {

    @Autowired
    private lateinit var service: FilesService

    @PostMapping(value = [""], produces = ["application/json"])
    fun upload(
        @Valid @ModelAttribute request: UploadRequest
    ): ResponseEntity<ReturnData> {
        return service.upload(request)
    }

    @RequestMapping(value = ["{id}"], method = [RequestMethod.GET])
    fun show(@PathVariable id: String, httpServletResponse: HttpServletResponse): ByteArray? {
        return service.show(id, httpServletResponse)
    }

    @RequestMapping(value = ["pdf/{id}"], method = [RequestMethod.GET])
    fun showPdf(@PathVariable id: String, httpServletResponse: HttpServletResponse): ByteArray? {
        return service.showPdf(id, httpServletResponse)
    }

    @RequestMapping(value = ["download"], method = [RequestMethod.GET])
    @ResponseBody
    fun download(
        @RequestParam("id") id: String,
        response: HttpServletResponse
    ): ByteArray? {
        return service.download(id, response)
    }

    @DeleteMapping(value = [""], produces = ["application/json"])
    fun deleteData(@Valid @RequestBody request: DeleteDataRequest): ResponseEntity<ReturnData> {
        return service.deleteData(request)
    }

    @RequestMapping(value = ["get-by-id"], method = [RequestMethod.GET], produces = ["application/json"])
    fun getById(@RequestParam id: String?): ResponseEntity<ReturnData> {
        return service.getById(id)
    }
}
