package id.go.kemenag.madrasah.pmrms.files.model.request

import javax.validation.constraints.NotEmpty

data class DeleteDataRequest(
    @field:NotEmpty
    var id: String? = null
)
