package id.go.kemenag.madrasah.pmrms.files.constant

import java.io.File

val ROOT_DIR = "${File.separator}pmrms-data${File.separator}"

//const val ROOT_DIR = "D:\\app-files\\pmrms\\"

val UPLOAD_DIR = "${ROOT_DIR}uploads${File.separator}"

val UPLOAD_FILES_DIR = "files${File.separator}"
