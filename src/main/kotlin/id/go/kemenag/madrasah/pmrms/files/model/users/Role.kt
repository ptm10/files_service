package id.go.kemenag.madrasah.pmrms.files.model.users

data class Role(

    var id: String? = null,

    var name: String? = null,

    var code: String? = null,

    var supervisiorId: String? = null
)
