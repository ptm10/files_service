package id.go.kemenag.madrasah.pmrms.files.model.request

import id.go.kemenag.madrasah.pmrms.files.constant.VALIDATOR_MSG_REQUIRED
import org.springframework.web.multipart.MultipartFile
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

data class UploadRequest(

    @field:NotEmpty(message = "Files $VALIDATOR_MSG_REQUIRED")
    var files: MutableList<MultipartFile>? = null,

    @field:NotNull(message = "Attachment Type $VALIDATOR_MSG_REQUIRED")
    var attachmentType: Int? = null
)

