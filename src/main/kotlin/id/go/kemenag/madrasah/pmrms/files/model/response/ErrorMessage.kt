package id.go.kemenag.madrasah.pmrms.files.model.response

data class ErrorMessage(
    var key: String? = null,
    var message: String? = null
)
