package id.go.kemenag.madrasah.pmrms.files.service

import com.aspose.cells.PdfCompliance
import com.aspose.cells.PdfSaveOptions
import com.aspose.cells.Workbook
import com.lowagie.text.Font
import fr.opensagres.xdocreport.itext.extension.font.IFontProvider
import id.go.kemenag.madrasah.pmrms.files.constant.*
import id.go.kemenag.madrasah.pmrms.files.helpers.*
import id.go.kemenag.madrasah.pmrms.files.model.request.DeleteDataRequest
import id.go.kemenag.madrasah.pmrms.files.model.request.UploadRequest
import id.go.kemenag.madrasah.pmrms.files.model.response.ErrorMessage
import id.go.kemenag.madrasah.pmrms.files.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.files.pojo.AttachmentType
import id.go.kemenag.madrasah.pmrms.files.pojo.Files
import id.go.kemenag.madrasah.pmrms.files.repository.AttachmentTypeRepository
import id.go.kemenag.madrasah.pmrms.files.repository.FileRepository
import org.apache.commons.io.FilenameUtils
import org.apache.commons.io.IOUtils
import org.apache.poi.xwpf.converter.pdf.PdfConverter
import org.apache.poi.xwpf.converter.pdf.PdfOptions
import org.apache.poi.xwpf.usermodel.XWPFDocument
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.io.*
import javax.servlet.http.HttpServletResponse


@Suppress("UNCHECKED_CAST")
@Service
class FilesService {

    @Autowired
    private lateinit var repo: FileRepository

    @Autowired
    private lateinit var repoAttachmentType: AttachmentTypeRepository

    private val title = "File"

    fun upload(request: UploadRequest): ResponseEntity<ReturnData> {
        return try {
            val validate = validateUpload(request)
            val listMessage = validate["listMessage"] as List<ErrorMessage>
            if (listMessage.isNotEmpty()) {
                return responseBadRequest(data = listMessage)
            }

            val attachmentType: AttachmentType = validate["attachmentType"] as AttachmentType
            var dirAttachment = attachmentType.fileType?.replace(" ", "_").toString().lowercase().trim()
            if (attachmentType.fileType.isNullOrEmpty()) {
                dirAttachment = "${System.currentTimeMillis()}"
            }

            val saved: MutableList<Files> = emptyList<Files>().toMutableList()
            request.files?.forEach {
                try {
                    saved.add(repo.save(uploadFile(it, UPLOAD_FILES_DIR + dirAttachment, attachmentType)))
                } catch (e: Exception) {
                    throw e
                }
            }
            responseSuccess(data = saved)
        } catch (e: Exception) {
            throw e
        }
    }

    private fun validateUpload(request: UploadRequest): MutableMap<String, Any> {
        val rData = mutableMapOf<String, Any>()
        try {
            val listMessage: MutableList<ErrorMessage> = emptyList<ErrorMessage>().toMutableList()

            val findAttachmentType = repoAttachmentType.findByTypeAndActive(request.attachmentType)
            if (!findAttachmentType.isPresent) {
                listMessage.add(
                    ErrorMessage(
                        "attachmentType",
                        "Attachment Type ${request.attachmentType}${VALIDATOR_MSG_NOT_FOUND}"
                    )
                )
            } else {
                rData["attachmentType"] = findAttachmentType.get()
            }

            var allowedMimeType: List<String> = emptyList()

            findAttachmentType.get().allowedMimeType?.let {
                allowedMimeType = it.split(",")
            }

            if (allowedMimeType.isEmpty()) {
                listMessage.add(ErrorMessage("attachmentType", VALIDATOR_MSG_NOT_VALID))
            }

            val limitFileSize = findAttachmentType.get().allowedSizeInKb ?: 0

            request.files?.forEach {
                if (!allowedMimeType.contains(it.contentType)) {
                    listMessage.add(ErrorMessage("files", "Mime Type ${it.originalFilename} $VALIDATOR_MSG_NOT_MATCH"))
                }

                val fileSize = it.size / 1024
                if (fileSize > limitFileSize) {
                    listMessage.add(
                        ErrorMessage(
                            "files",
                            "Ukuran file tidak boleh lebih besar dari ${limitFileSize} KB"
                        )
                    )
                }
            }

            rData["listMessage"] = listMessage
        } catch (e: Exception) {
            throw e
        }

        return rData
    }

    fun show(id: String, httpServletResponse: HttpServletResponse): ByteArray? {
        var fis: FileInputStream? = null
        try {
            val dir: String
            val find = repo.findById(id)
            if (!find.isPresent) {
                responseNotFound(httpServletResponse)
            }
            val data = find.get()
            dir = UPLOAD_DIR + data.filepath + File.separator + data.filename

            val file = File(dir)
            if (file.exists()) {
                data.mimeType?.let {
                    httpServletResponse.setHeader("Content-Type", data.mimeType)
                }
                fis = FileInputStream(file)
                return IOUtils.toByteArray(fis)
            }

            responseNotFound(httpServletResponse)

            return null
        } catch (e: Exception) {
            throw e
        } finally {
            fis?.close()
        }
    }

    fun showPdf(id: String, httpServletResponse: HttpServletResponse): ByteArray? {
        var fis: FileInputStream? = null
        try {
            val dir: String
            val find = repo.findById(id)
            if (!find.isPresent) {
                responseNotFound(httpServletResponse)
            }
            val data = find.get()
            dir = UPLOAD_DIR + data.filepath + File.separator

            val file = File(dir + data.filename)
            if (file.exists()) {
                httpServletResponse.addHeader(
                    "Content-Disposition",
                    "attachment; filename=${file.nameWithoutExtension}.pdf"
                )

                data.mimeType?.let {
                    httpServletResponse.addHeader("Content-Type", "application/pdf")
                }


                var fileDir = file.absolutePath

                if (FilenameUtils.getExtension(file.name) != "pdf") {
                    fileDir = "$dir${file.nameWithoutExtension}.pdf"
                    if (!File(fileDir).exists()) {
                        if (FilenameUtils.getExtension(file.name).contains("xls")) {
                            val workbook = Workbook(file.absolutePath)
                            val options = PdfSaveOptions()
                            options.compliance = PdfCompliance.PDF_A_1_A
                            workbook.save(fileDir, options)
                        }

                        if (FilenameUtils.getExtension(file.name).contains("doc")) {
                            return docxToPdf(file.inputStream())
                        }
                    }
                }

                fis = FileInputStream(File(fileDir))
                return IOUtils.toByteArray(fis)
            }

            responseNotFound(httpServletResponse)

            return null
        } catch (e: Exception) {
            throw e
        } finally {
            fis?.close()
        }
    }

    fun download(id: String, response: HttpServletResponse): ByteArray? {
        var fis: FileInputStream? = null
        return try {
            val dir: String
            val find = repo.findById(id)
            if (!find.isPresent) {
                responseNotFound(response)
            }

            val data = find.get()
            dir = UPLOAD_DIR + data.filepath + File.separator + data.filename

            val file = File(dir)
            if (!file.exists()) {
                responseNotFound(response)
            }
            response.setHeader("Content-Disposition", "attachment; filename=${data.filename}")
            fis = FileInputStream(file)
            IOUtils.toByteArray(fis)
        } catch (e: Exception) {
            e.printStackTrace()
            e.message!!.toByteArray()
        } finally {
            if (fis != null) {
                try {
                    fis.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }
    }

    fun deleteData(request: DeleteDataRequest): ResponseEntity<ReturnData> {
        try {
            val find = repo.findById(request.id ?: "")
            if (!find.isPresent) {
                return responseNotFound(message = "$title $VALIDATOR_MSG_NOT_FOUND")
            }
            val data = find.get()
            deleteFile(UPLOAD_DIR + data.filepath + File.separator + data.filename)
            repo.delete(data)
            return responseSuccess()
        } catch (e: Exception) {
            throw e
        }
    }

    fun getById(id: String?): ResponseEntity<ReturnData> {
        try {
            val find = repo.findById(id ?: "")
            if (find.isPresent) {
                return responseSuccess(data = find.get())
            }
            return responseNotFound()
        } catch (e: Exception) {
            throw e
        }
    }

    @Throws(java.lang.Exception::class)
    fun docxToPdf(docxStream: InputStream?): ByteArray? {
        var targetStream: ByteArrayOutputStream? = null
        val doc: XWPFDocument?
        return try {
            doc = XWPFDocument(docxStream)
            val options = PdfOptions.create()
            // Chinese font processing
            options.fontProvider(IFontProvider { familyName, encoding, size, style, color ->
                try {
                    val fontChinese = Font()
                    fontChinese.color = color
                    fontChinese.style = style
                    fontChinese.size = size

                    if (familyName != null) fontChinese.setFamily(familyName)
                    return@IFontProvider fontChinese
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                    return@IFontProvider null
                }
            })
            targetStream = ByteArrayOutputStream()
            PdfConverter.getInstance().convert(doc, targetStream, options)
            targetStream.toByteArray()
        } catch (e: IOException) {
            throw java.lang.Exception(e)
        } finally {
            IOUtils.closeQuietly(targetStream)
        }
    }
}
