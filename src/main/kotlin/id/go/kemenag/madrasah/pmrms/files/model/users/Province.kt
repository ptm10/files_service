package id.go.kemenag.madrasah.pmrms.files.model.users

data class Province(

    var id: String? = null,

    var name: String? = null,

    var code: String? = null
)
