package id.go.kemenag.madrasah.pmrms.files.helpers

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import id.go.kemenag.madrasah.pmrms.files.constant.HEADER_STRING
import id.go.kemenag.madrasah.pmrms.files.model.response.ReturnData
import kong.unirest.Unirest

class RequestHelpers {

    companion object {

        fun authDetail(authUrl: String, bearer: String): ReturnData? {
            return try {
                val objectMapper = ObjectMapper()
                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

                val reqUrl = "$authUrl/auth/detail"
                val response = Unirest.get(reqUrl)
                    .header("Content-Type", "application/json")
                    .header(HEADER_STRING, bearer)
                    .asString()

                objectMapper.readValue(response.body, ReturnData::class.java)
            } catch (e: Exception) {
                e.printStackTrace()
                null
            }
        }

    }
}
