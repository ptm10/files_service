package id.go.kemenag.madrasah.pmrms.files.constant

const val TOKEN_PREFIX = "Bearer "
const val HEADER_STRING = "Authorization"

val USER_ADMIN_ALLOWED_PATH =
    listOf(
        "/files/*"
    )

val AUDIENCE_FILTER_PATH = mapOf(
    "user-admin" to USER_ADMIN_ALLOWED_PATH
)
